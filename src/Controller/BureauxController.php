<?php

namespace App\Controller;

use App\Entity\Bureau;
use App\Entity\Ligue;
use App\Entity\Organisation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class BureauxController extends AbstractController
{
    public function listeBureauxDisponibles()
    {
        $bureaux = $this->getDoctrine()->getRepository(Bureau::class)->findByDispo();
        return $this->render('listeBureaux.html.twig',['titre'=>"Liste des Bureaux disponibles",'bureaux'=>$bureaux]);
    }

    public function listeBureauxNonDisponibles()
    {
        $bureaux = $this->getDoctrine()->getRepository(Bureau::class)->findByNonDispo();
        return $this->render('listeBureaux.html.twig',['titre'=>"Liste des Bureaux non disponibles",'bureaux'=>$bureaux]);
    }

    public function attribuerBureau($id, Request $request)
    {
        $bureau = $this->getDoctrine()->getRepository(Bureau::class)->find($id);

        $formUpdt = $this->createFormBuilder()
            ->add('Ligue', EntityType::class, [
                'class' => Ligue::class,
                'required'   => false,
                'choice_label' => 'nom',
                'placeholder' => 'bureaux vide'])
            ->add('attribuer', SubmitType::class)
            ->getForm();

        $formUpdt->handleRequest($request);


        if ($formUpdt->isSubmitted()) {  //ce code est exécuté lors de la soumission du formulaire

            $entityManager = $this->getDoctrine()->getManager();

            $ligue = $formUpdt->getData()['Ligue'];

            if($ligue != null){
                $id = $ligue->getId();

                $ligue = $entityManager->getRepository(Organisation::class)->find($id);

            }
            $bureau->setOccupant($ligue);
            $entityManager->flush();
            return $this->redirectToRoute('listeBureauxDisponibles');
        }

        return $this->render('attribuerBureauLigue.html.twig',['bureau'=>$bureau,'formUpdt' => $formUpdt->createView()]);
    }
}
