<?php

namespace App\Controller;

use App\Entity\Ligue;
use App\Entity\Organisation;
use App\Entity\SalleResa;
use App\Form\AttributionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;



class SalleResaController extends AbstractController
{
    /**
     * Require ROLE_ADMIN
     * @IsGranted("ROLE_ADMIN")
     */
    public function attributionSalleResaCat(Request $request)
    {

        $formSalleResa = $this->createForm(AttributionType::class);

        $formSalleResa ->handleRequest($request);


        if ($formSalleResa->isSubmitted()) {  //ce code est exécuté lors de la soumission du formulaire

            $entityManager = $this->getDoctrine()->getManager();

            $salleresa = $formSalleResa->getData()['nom'];
            $categorie = $formSalleResa->getData()['libelle'];

            $salleresa->setCategorie($categorie);
            $entityManager->flush();

            return $this->redirect('../attributionSalleResa');
        }

        return $this->render('FormSalleResaAffectCat.html.twig', ['formSalleResa' => $formSalleResa->createView()]);
    }

    public function salleresas()
    {
        $salleresas = $this->getDoctrine()->getRepository(SalleResa::class)->findAll();
        return $this->render('listeSalleresa.html.twig', ['titre' => "Liste des Salles Reservables", 'salleresas' => $salleresas]);
    }

    public function detailsalle($id)
    {
        $saller = $this->getDoctrine()->getRepository(SalleResa::class)->find($id);
        return $this->render('detailsalleresa.html.twig',['titre'=>"Liste des Salles Reservable",'saller'=>$saller]);

    }
}