<?php

namespace App\Controller;

use App\Entity\Categorie;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RechercheController extends AbstractController
{
    public function recherchParSalleCat(Request $request)
    {
        $formRechSalleCat = $this->createFormBuilder()
            ->add('categorie', EntityType::class, [
                'class' => Categorie::class])
            ->add('rechercher', SubmitType::class)
            ->getForm();

        $formRechSalleCat->handleRequest($request);

        if ($formRechSalleCat->isSubmitted()) {  //ce code est exécuté lors de la soumission du formulaire


            $recherche = $formRechSalleCat->getData();
            $categorie = $formRechSalleCat->getData()['categorie'];


            return $this->redirectToRoute('uncategorie',['id' => ($categorie->getId())]);
        }

        return $this->render('rechercherParSalleCat.html.twig',['formRechSalleCat' => $formRechSalleCat->createView()]);
    }
}