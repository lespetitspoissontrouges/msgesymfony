<?php

namespace App\Controller;

use App\Entity\Organisation;
use App\Form\LigueType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;


class OrganisationController extends AbstractController
{
    public function ListeFormulaires()

    {
        return $this->render('ListeFormulaires.html.twig');
    }

    public function majLigue(int $id, Request $request)
    {

        //$user = $this->getUser();
        $ligue = $this->getDoctrine()->getRepository(Organisation::class)->find($id);

        $formLigue = $this->createForm(LigueType::class, $ligue);


        $entityManager = $this->getDoctrine()->getManager();

        //$ligue = $entityManager->getRepository(Organisation::class)->find($id);   //UNIQUEMENT POUR INSERT


        $formLigue->handleRequest($request); // $request est un objet transmis en paramètre de la fonction


        if ($formLigue->isSubmitted()) {

            //$ligue -> setAdresse($formLigue->getData()['adresse']);   //UNIQUEMENT POUR INSERT
            //$ligue -> setMail($formLigue->getData()['mail']);         :
            //$ligue -> setTel($formLigue->getData()['tel']);           :

            $entityManager->persist($ligue); //stocké en mémoire dans la collection de livres
            $entityManager->flush(); // synchronisation avec la BDD -> production d'un ordre SQL de type INSERT

            // [code de traitement du formulaire une fois soumis : récupération des données] //A compléter

            return $this->redirectToRoute('formMAJredirect', ['id' => $id]); //A compléter
        }

        // Construction de la réponse du formulaire non soumis -- affichage du formulaire
        return $this->render('FormMAJ.html.twig', ['formLigue' => $formLigue->createView()]);//A compléter

    }
}