<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Form\CategorieType;
use App\Form\TarifType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



class CategorieController extends AbstractController
{
    /**
     * Require ROLE_ADMIN
     * @IsGranted("ROLE_ADMIN")
     */
    public function ajoutCat (Request $request) {


        //$categorie = new Categorie();
        $formCat = $this->createForm(CategorieType::class);

        $formCat->handleRequest($request); // $request est un objet transmis en paramètre de la fonction


        if ($formCat->isSubmitted()) {


            $entityManager = $this->getDoctrine()->getManager();
            //$entityManager->persist($categorie); //stocké en mémoire dans la collection de livres
            $entityManager->flush(); // synchronisation avec la BDD -> production d'un ordre SQL de type INSERT

            // [code de traitement du formulaire une fois soumis : récupération des données] //A compléter


            //return $this->redirect('/formCategorie'); //A compléter
        }

        // Construction de la réponse du formulaire non soumis -- affichage du formulaire
        return $this->render('FormAddCategorie.html.twig', ['formCat' => $formCat->createView()]);//A compléter

    }

    /**
     * Require ROLE_ADMIN
     * @IsGranted("ROLE_ADMIN")
     */
    public function modifTarif (Request $request) {


        $tarif = new Categorie();

        $formTarif = $this->createForm(TarifType::class,$tarif);

        $formTarif->handleRequest($request); // $request est un objet transmis en paramètre de la fonction


        if ($formTarif->isSubmitted()) {

            $entityManager = $this->getDoctrine()->getManager();
            $categorie = $entityManager->getRepository(Categorie::class)->findOneBy(['libelle'=>$tarif->getLibelle()]);
            $categorie->setTarif($tarif->getTarif());
            $entityManager->flush(); // synchronisation avec la BDD -> production d'un ordre SQL de type INSERT

            // [code de traitement du formulaire une fois soumis : récupération des données] //A compléter
            return $this->redirect('../formTarif'); //A compléter
        }

        // Construction de la réponse du formulaire non soumis -- affichage du formulaire
        return $this->render('FormMAJTarif.html.twig', ['formTarif' => $formTarif->createView()]);//A compléter

    }

    /**
     * @Route("/categories", name="categories")
     */
    public function categories(): Response
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        return $this->render('categories.html.twig',['titre'=>"Liste des Categories",'categories'=>$categories]);
    }
    public function afficherCategorie($id)
    {
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->find($id);
        return $this->render('detailscategorie.html.twig',['categorie'=>$categorie]);
    }

}