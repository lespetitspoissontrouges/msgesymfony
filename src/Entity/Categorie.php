<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 * @ORM\Table(name="categorie")
 */
class Categorie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="categorie_id_seq")
     * @ORM\Column(type="integer",name="id")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */

    private $libelle;



    /**
     * @ORM\Column(type="integer")
     */

    private $tarif;



    public function __toString()
    {
        return $this->getLibelle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getTarif(): ?int
    {
        return $this->tarif;
    }

    public function setTarif(int $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SalleResa", mappedBy="categorie")
     */
    private $lesSallesResas;

    public function __construct()
    {
        $this->lesSallesResas = new ArrayCollection();
    }

    /**
     * @return Collection|SalleResa[]
     */
    public function getLesSallesResas(): Collection
    {
        return $this->lesSallesResas;
    }

    public function addLesSallesResa(SalleResa $lesSallesResa): self
    {
        if (!$this->lesSallesResas->contains($lesSallesResa)) {
            $this->lesSallesResas[] = $lesSallesResa;
            $lesSallesResa->setcategorie($this);
        }

        return $this;
    }

    public function removeLesSallesResa(SalleResa $lesSallesResa): self
    {
        if ($this->lesSallesResas->removeElement($lesSallesResa)) {
            // set the owning side to null (unless already changed)
            if ($lesSallesResa->getcategorie() === $this) {
                $lesSallesResa->setcategorie(null);
            }
        }

        return $this;
    }


}