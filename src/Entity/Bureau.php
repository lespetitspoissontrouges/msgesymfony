<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**

 * @ORM\Entity(repositoryClass="App\Repository\BureauRepository")
 * @ORM\Table(name="bureau")

 */
class Bureau extends Salle
{

    /**
     *
     * @ORM\ManyToOne(targetEntity="Organisation")
     * @ORM\JoinColumn(name="occupant", referencedColumnName="id")
     */
    private $occupant;

    protected $id;

    public function getOccupant(): ?Organisation
    {
        return $this->occupant;
    }

    public function setOccupant(?Organisation $occupant): self
    {
        $this->occupant = $occupant;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

}