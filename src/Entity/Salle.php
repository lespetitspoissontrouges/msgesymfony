<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SalleRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"salle" = "Salle", "bureau" = "Bureau", "salleresa" = "SalleResa"})
 * @ORM\Table(name="salle")
 */

class Salle

{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="salle_id_seq")
     * @ORM\Column(type="integer",name="id")
     */

    protected $id;

    /**
     * @ORM\Column(type="string")
     */

    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Etage", inversedBy="lesSalles")
     * @ORM\JoinColumn(name="situation", referencedColumnName="id")
     */

    private $situation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function __toString()
    {
        return $this->getNom();
    }

    public function getSituation(): ?Etage
    {
        return $this->situation;
    }

    public function setSituation(?Etage $situation): self
    {
        $this->situation = $situation;

        return $this;
    }


}