<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**

 * @ORM\Entity(repositoryClass="App\Repository\BatimentRepository")

 * @ORM\Table(name="Batiment")

 */

class Batiment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer",name="id")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $nom;

    public function __construct()
    {
        $this->lesSalles = new ArrayCollection();
        $this->nom = new ArrayCollection();
        $this->lesEtages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Etage", mappedBy="batiment")
     */
    private $lesEtages;

    public function addNom(Salle $nom): self
    {
        if (!$this->nom->contains($nom)) {
            $this->nom[] = $nom;
            $nom->addLesBatiment($this);
        }

        return $this;
    }

    public function removeNom(Salle $nom): self
    {
        if ($this->nom->removeElement($nom)) {
            $nom->removeLesBatiment($this);
        }

        return $this;
    }

    /**
     * @return Collection|Etage[]
     */
    public function getLesEtages(): Collection
    {
        return $this->lesEtages;
    }

    public function addLesEtage(Etage $lesEtage): self
    {
        if (!$this->lesEtages->contains($lesEtage)) {
            $this->lesEtages[] = $lesEtage;
            $lesEtage->setBatiment($this);
        }

        return $this;
    }

    public function removeLesEtage(Etage $lesEtage): self
    {
        if ($this->lesEtages->removeElement($lesEtage)) {
            // set the owning side to null (unless already changed)
            if ($lesEtage->getBatiment() === $this) {
                $lesEtage->setBatiment(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getNom();
    }


}