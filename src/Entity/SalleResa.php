<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SalleResaRepository")
 * @ORM\Table(name="salleresa")
 */

class SalleResa extends Salle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="salleresa_id_seq")
     * @ORM\Column(type="integer",name="id")
     */

    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Organisation")
     * @ORM\JoinColumn(name="idoccupant", referencedColumnName="id")
     */

    private $idoccupant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="lesSallesResas")
     * @ORM\JoinColumn(name="categorie", referencedColumnName="id")
     */

    private $categorie;

    /**
     * @ORM\Column(type="string")
     */

    private $reservation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getIdoccupant(): ?Organisation
    {
        return $this->idoccupant;
    }

    public function setIdoccupant(?Organisation $idoccupant): self
    {
        $this->idoccupant = $idoccupant;

        return $this;
    }

    public function __toString()
    {
        return strval($this->getId());
    }

    public function getReservation(): ?string
    {
        return $this->reservation;
    }

    public function setReservation(string $reservation): self
    {
        $this->reservation = $reservation;

        return $this;
    }

}


