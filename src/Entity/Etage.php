<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EtageRepository")
 * @ORM\Table(name="etage")
 */
class Etage
{
    /**

     * @ORM\Id
     * @ORM\Column(type="integer",name="id")

     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $numero;

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function setNumero(int $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Salle", mappedBy="situation")
     */
    private $lesSalles;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Batiment", inversedBy="lesEtages")
     * @ORM\JoinColumn(name="batiment", referencedColumnName="id")
     */
    private $batiment;

    public function __construct()
    {
        $this->lesSalles = new ArrayCollection();
    }

    /**
     * @return Collection|Salle[]
     */
    public function getLesSalles(): Collection
    {
        return $this->lesSalles;
    }

    public function addLesSalle(Salle $lesSalle): self
    {
        if (!$this->lesSalles->contains($lesSalle)) {
            $this->lesSalles[] = $lesSalle;
            $lesSalle->setSituation($this);
        }

        return $this;
    }

    public function removeLesSalle(Salle $lesSalle): self
    {
        if ($this->lesSalles->removeElement($lesSalle)) {
            // set the owning side to null (unless already changed)
            if ($lesSalle->getSituation() === $this) {
                $lesSalle->setSituation(null);
            }
        }

        return $this;
    }

    public function getBatiment(): ?Batiment
    {
        return $this->batiment;
    }

    public function setBatiment(?Batiment $batiment): self
    {
        $this->batiment = $batiment;

        return $this;
    }

    public function __toString()
    {
        return strval($this->getNumero());
    }





}