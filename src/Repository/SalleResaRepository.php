<?php

namespace App\Repository;

use App\Entity\SalleResa;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SalleResa|null find($id, $lockMode = null, $lockVersion = null)
 * @method SalleResa|null findOneBy(array $criteria, array $orderBy = null)
 * @method SalleResa[]    findAll()
 * @method SalleResa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalleResaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalleResa::class);
    }

    // /**
    //  * @return SalleResa[] Returns an array of SalleResa objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SalleResa
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function reserverSalle($id) {
        $qb = $this->getEntityManager()->createQueryBuilder('s');
        $qb ->update(SalleResa::class,'s')
            ->set('s.reservation',':reservation')
            ->where("s.id IN (:id")
            ->setParameter('reservation','en attente')
            ->setParameter('id',$id);
        $query = $qb->getQuery();
        return $query->execute();

    }
}

