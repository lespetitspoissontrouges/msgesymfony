<?php
namespace App\Form;

use App\Entity\Categorie;
use App\Entity\SalleResa;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AttributionType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',EntityType::class,[
                'class'=> SalleResa::class,
                'choice_label'=> 'nom'])
            ->add('libelle',EntityType::class,[
                'class'=> Categorie::class])
            ->add('enregistrer', SubmitType::class);

        //code créant le formulaire
    }

    // Bien que cela ne soit pas toujours nécessaire, il est recommandé d'ajouter
    // explicitement à travers l'option data_class la classe concernée par le FormType.


    //public function configureOptions(OptionsResolver $resolver){

      //  $resolver->setDefaults(['data_class' => SalleResa::class]);

    //}
}