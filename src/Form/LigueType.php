<?php
namespace App\Form;

use App\Entity\Organisation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LigueType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('adresse')
            ->add('mail')
            ->add('tel')
            ->add('enregistrer', SubmitType::class);

        //code créant le formulaire
    }


    // Bien que cela ne soit pas toujours nécessaire, il est recommandé d'ajouter
    // explicitement à travers l'option data_class la classe concernée par le FormType.

    public function configureOptions(OptionsResolver $resolver){

        $resolver->setDefaults(['data_class' => Organisation::class]);

    }

}