<?php
namespace App\Form;

use App\Entity\Categorie;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TarifType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', EntityType::class, [
                'class' => Categorie::class])
            ->add('tarif')
            ->add('enregistrer', SubmitType::class);

        //code créant le formulaire
    }

    // Bien que cela ne soit pas toujours nécessaire, il est recommandé d'ajouter
    // explicitement à travers l'option data_class la classe concernée par le FormType.

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(['data_class' => Categorie::class]);

    }
}