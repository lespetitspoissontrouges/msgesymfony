CREATE OR REPLACE FUNCTION tarifMinEtMax() RETURNS TRIGGER AS
$$
BEGIN
	IF NEW.tarif > 0 AND NEW.tarif <= 150 THEN
		RETURN NEW;
    ELSE
		RETURN NULL;
    END IF;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER trig_bef_ins_categorie BEFORE INSERT OR UPDATE ON categorie
FOR EACH ROW
EXECUTE PROCEDURE tarifMinEtMax();



CREATE OR REPLACE FUNCTION troisBureauxParLigueMax() RETURNS TRIGGER AS
$$
DECLARE
    nbBureauxParLigue integer;
BEGIN
    SELECT INTO nbBureauxParLigue count(id) FROM bureau
    WHERE occupant = NEW.occupant;
    IF nbBureauxParLigue >= 3 THEN
        RETURN NULL;
    ELSE
        RETURN NEW;
    END IF;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER trig_bef_ins_bureau BEFORE UPDATE ON bureau
FOR EACH ROW
EXECUTE PROCEDURE troisBureauxParLigueMax();