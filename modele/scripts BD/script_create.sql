

-------------------- M2L -----------------------------------------


-- lespetitspoissontrouges.batiment definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.batiment;

CREATE TABLE lespetitspoissontrouges.batiment (
	id serial NOT NULL,
	nom varchar NULL,
	CONSTRAINT batiment_pk PRIMARY KEY (id)
);


-- lespetitspoissontrouges.categorie definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.catgorie;

CREATE TABLE categorie (
	id serial NOT NULL,
	libelle varchar NOT NULL,
	tarif integer not null,
	CONSTRAINT categorie_pk PRIMARY KEY (id)
);


-- lespetitspoissontrouges.etage definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.etage;

CREATE TABLE lespetitspoissontrouges.etage (
	id serial NOT NULL,
	numero int2 NOT NULL,
	batiment int4 NOT NULL,
	CONSTRAINT etage_pk PRIMARY KEY (id),
	CONSTRAINT etage_un UNIQUE (numero, batiment),
	CONSTRAINT etage_batiment_fk FOREIGN KEY (batiment) REFERENCES batiment(id)
);


-- lespetitspoissontrouges.salle definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.salle;

CREATE TABLE lespetitspoissontrouges.salle (
	id serial NOT NULL,
	nom varchar NULL,
	situation int4 NULL,
    "type" varchar(255) NULL,
	CONSTRAINT salle_pk PRIMARY KEY (id),
	CONSTRAINT salle_etage_fk FOREIGN KEY (situation) REFERENCES etage(id)
);

-- Drop table

-- DROP TABLE lespetitspoissontrouges.salleresa;

CREATE TABLE lespetitspoissontrouges.salleresa (
    id int4 NOT NULL,
    idoccupant int4 NULL,
    categorie int4 NULL,
    reservation varchar(50) NULL,
    CONSTRAINT salleresa_pk PRIMARY KEY (id),
    CONSTRAINT salleresa_fk FOREIGN KEY (categorie) REFERENCES categorie(id),
    CONSTRAINT salleresa_organisation_fk FOREIGN KEY (idoccupant) REFERENCES organisation(id),
    CONSTRAINT salleresa_salle_fk FOREIGN KEY (id) REFERENCES salle(id)
);


-- lespetitspoissontrouges.reservation definition

-- Drop table

-- DROP TABLE reservation;

CREATE TABLE reservation (
    id int4 NOT NULL,
    heure date NULL,
    idsalleresa int4 NULL,
    idorganisation int4 NULL,
    CONSTRAINT fk_reservation_organisation FOREIGN KEY (idorganisation) REFERENCES organisation(id),
    CONSTRAINT fk_reservation_salleresa FOREIGN KEY (idsalleresa) REFERENCES salleresa(id),
    CONSTRAINT reservation_pkey PRIMARY KEY (id)
);



-- lespetitspoissontrouges.discipline definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.discipline;

CREATE TABLE lespetitspoissontrouges.discipline (
	id serial NOT NULL,
	libelle varchar NOT NULL,
	olympique varchar not null,
	CONSTRAINT discipline_pk PRIMARY KEY (id)
);



-- lespetitspoissontrouges.structure definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.organisation;

CREATE TABLE lespetitspoissontrouges.organisation (
	
	id serial not null,
	nom varchar NOT NULL,
	adresse varchar NULL,
	nomfichier varchar NULL,
	iddiscipline int not NULL,
	mail varchar(50) NULL,
	tel varchar(50) NULL,
    "type" varchar(50) NULL,
	CONSTRAINT organisation_pk PRIMARY KEY (id),
	CONSTRAINT organisation_discipline_fk FOREIGN KEY (iddiscipline) REFERENCES discipline(id)
	
);



-- lespetitspoissontrouges.bureau definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.bureau;

CREATE TABLE lespetitspoissontrouges.bureau (
	id int4 NOT NULL,
	occupant int4 NULL,
	CONSTRAINT bureau_pk PRIMARY KEY (id),
	CONSTRAINT bureau_organisation_fk FOREIGN KEY (occupant) REFERENCES organisation(id),
	CONSTRAINT bureau_salle_fk FOREIGN KEY (id) REFERENCES salle(id)
);


-- lespetitspoissontrouges.ligue definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.ligue;

CREATE TABLE lespetitspoissontrouges.ligue (
	
	id serial not null, 
	CONSTRAINT ligue_pk PRIMARY KEY (id),
	CONSTRAINT ligue_organisation_fk FOREIGN KEY (id) REFERENCES organisation(id)

);


-- lespetitspoissontrouges.comite definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.comite;

CREATE TABLE lespetitspoissontrouges.comite (
	id serial NOT NULL,
	idligue int NOT NULL,
	departement varchar(255) NULL,
	CONSTRAINT comite_pk PRIMARY KEY (id),
	CONSTRAINT comite_organisation_fk FOREIGN KEY (id) REFERENCES organisation(id)
);

-- lespetitspoissontrouges.club definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.club;

CREATE TABLE lespetitspoissontrouges.club (
	id serial NOT NULL,
	idComite int NOT NULL,
	site varchar(255) NULL,
	lieupract varchar (255) null,
	CONSTRAINT club_pk PRIMARY KEY (id),
	CONSTRAINT club_organisation_fk FOREIGN KEY (id) REFERENCES organisation (id)
);



-- lespetitspoissontrouges.roles  definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.roles;

CREATE TABLE lespetitspoissontrouges.roles (
	id serial NOT NULL,
	libelle varchar,
	CONSTRAINT role_pk PRIMARY KEY (id)

);



-- lespetitspoissontrouges.accounts definition

-- Drop table

-- DROP TABLE accounts;

CREATE TABLE accounts (
    id serial NOT NULL,
    username varchar NOT NULL,
    "password" varchar NOT NULL,
    roles json NULL,
    email varchar(255) NULL,
    nom varchar(255) NULL,
    prenom varchar(255) NULL,
    CONSTRAINT accounts_pk PRIMARY KEY (id)
);


-- lespetitspoissontrouges.membres definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.membres;

CREATE TABLE lespetitspoissontrouges.membres (
	id serial NOT NULL,
	idaccount int NULL,
	nom varchar NOT NULL,
	mail varchar NULL,
	CONSTRAINT membres_pk PRIMARY KEY (id),
	CONSTRAINT membres_fk FOREIGN KEY (idaccount) REFERENCES Accounts(id)
);


-- lespetitspoissontrouges.composer  definition

-- Drop table

-- DROP TABLE lespetitspoissontrouges.composer;

CREATE TABLE lespetitspoissontrouges.composer (
	idroles int not null,
	idorg int not null,
	idmembre int not null,
	CONSTRAINT composer_pk PRIMARY KEY (idmembre, idorg, idroles),
	CONSTRAINT composer_fk FOREIGN KEY (idorg) REFERENCES organisation(id),
	CONSTRAINT composer1_fk FOREIGN KEY (idmembre) REFERENCES membres(id),
	CONSTRAINT composer2_fk FOREIGN KEY (idroles) REFERENCES roles(id)
);


------------------------------------------------------------------


